$(document).foundation();

$(function ($) {

    // -============================================
    // = GA-Tracking & open external links "_blank"
    // -============================================
    $('a').click(function () {
        var $a = $(this);
        var href = ($a.attr('href')) ? $a.attr('href') : '';

        if ((href.match(/^http/i)) && (! href.match(document.domain))) {
            $a.attr('target', '_blank');
            var category = 'outgoing';
            var event = 'click';
            _gaq.push(['_trackEvent', category, event, href]);
        } else {
            if (href.match(/.(doc|pdf|xls|ppt|zip|txt|vsd|vxd|js|css|rar|exe|wma|mov|avi|wmv|mp3)$/i)) {
                var category = 'download';
                var event = 'click';
                _gaq.push(['_trackEvent', category, event, href]);
            } else {
                if (href.match(/^mailto:/i)) {
                    var category = 'mailto';
                    var event = 'click';
                    _gaq.push(['_trackEvent', category, event, href]);
                }
            }
        }
    });

    // Video on homepage
    $('.video').parent().click(function () {
        if ($(this).children('.video').get(0).paused) {
            $(this).children('.video').get(0).play();
            $(this).children('.playpause').fadeOut();
        } else {
            $(this).children('.video').get(0).pause();
            $(this).children('.playpause').fadeIn();
        }
    });

    $('.video').on('loadstart', function (event) {
        $(this).addClass('loading')
    });
    $('.video').on('canplay', function (event) {
        $(this).removeClass('loading')
    });


    // powermail form
    var seniors = $('#powermail_field_interest_1'),
        tower = $('#powermail_field_interest_2'),
        villa = $('#powermail_field_interest_3'),
        receiverText = $('#receiver_text'),
        vendor = $('#powermail_field_vendor');

    if ($('.powermail_fieldwrap_flat input').val() != '') {
        $('.powermail_fieldwrap_flat').toggle();
        $('.powermail_fieldwrap_flat input').prop('readonly', true);
        if (vendor.val() == 'prs') {
            receiverText.text('Ihre Nachricht wird an die Paul-Riebeck-Stiftung gesendet.');
        } else {
            receiverText.text('Ihre Nachricht wird an die BWG Halle-Merseburg gesendet.');
        }
    } else {
        $('.powermail_fieldwrap_interest').toggle();
    };

    $('.powermail_fieldwrap_interest input[type=checkbox]').change(function () {
        if (seniors.is(':checked')) {
            receiverText.text('Ihre Nachricht wird an die Paul-Riebeck-Stiftung gesendet.');
            vendor.val('prs');
        } else {
            receiverText.text('Ihre Nachricht wird an die BWG Halle-Merseburg gesendet.');
            vendor.val('bwg');
        }
        if (!(seniors.is(':checked') || tower.is(':checked') || villa.is(':checked')) || (seniors.is(':checked') && (tower.is(':checked') || villa.is(':checked')))) {
            receiverText.text('Ihre Nachricht wird an die BWG Halle-Merseburg und die Paul-Riebeck-Stiftung gesendet.');
            vendor.val('');
        }
    });

});(jQuery);

// respect height of fixed menu when scrolling to anchor
function scrollToAnchor(hash)
{
    var target = $(hash),
        headerHeight = $('.sticky-topbar').height() + 30; // Get fixed header height

    target = target.length ? target : $('[name=' + hash.slice(1) +']');

    if (target.length) {
        $('html,body').animate({
            scrollTop: target.offset().top - headerHeight
        }, 500);
        return false;
    }
}

$(window).on('load', function ()
{ // fire event when page is completely loaded
    if (window.location.hash) {
        scrollToAnchor(window.location.hash);
    }

    $('a[href*=\\#]:not([href=\\#], .tabs-title a)').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            || location.hostname == this.hostname)
        {
            scrollToAnchor(this.hash);
        }
    });
});
