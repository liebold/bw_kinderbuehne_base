<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'BWG: Kinderbuehne Halle',
    'description' => 'Templates for kinderbuehne-halle.de',
    'category' => 'templates',
    'author' => 'Thomas Krüger',
    'author_email' => 'th.krueger@blueways.de',
    'author_company' => 'blueways.de',
    'shy' => '',
    'dependencies' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.9-8.7.99',
            'extbase' => '',
            'fluid' => '',
            'filemetadata' => '',
            'sms_responsive_images' => '',
            'vhs' => '',
            'bw_bwg_common' => ''
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'suggests' => [
    ],
];
