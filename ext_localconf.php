<?php
defined('TYPO3_MODE') or die();

/***************
 * Add RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['kinderbuehneDefault'] = 'EXT:' . $_EXTKEY .
                                                                   '/Configuration/RTE/Default.yaml';
