<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = $GLOBALS['_EXTKEY'] = 'bw_parkviertel_base';

// Configure new fields:
$temporaryColumns = [
    'tx_bwparkviertelbase_anchor' => [
        'label' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:tt_content.anchor',
        'exclude' => 0,
        'config' => [
            'type' => 'input',
            'max' => 30,
            'eval' => 'trim'
        ],
    ],
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $temporaryColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'tx_bwparkviertelbase_anchor',
    'after:colPos'
);
