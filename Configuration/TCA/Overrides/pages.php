<?php
defined('TYPO3_MODE') or die();

$_EXTKEY = $GLOBALS['_EXTKEY'] = 'bw_kinderbuehne_base';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $_EXTKEY,
    'Configuration/BackendLayouts/BackendLayouts.tsconfig',
    'Kinderbuehne-BackendLayouts'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $_EXTKEY,
    'Configuration/TSconfig/Page.tsconfig',
    'Kinderbuehne-PageTS'
);
